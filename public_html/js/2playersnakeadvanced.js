/*----------------------------------------------------------------------------
 * Variables
 * ---------------------------------------------------------------------------
 */

var snake;
var snakeLength;
var snakeSize;
var snakeDirection;
var snake2;
var snakeLength2;
var snakeSize2;
var snakeDirection2;

var food;

var context;
var screenWidth;
var screenHeight;

var gameState;
var gameOverMenu;
var restartButton;
var playHUD;
var scoreboard;
var scoreboard2;

/*----------------------------------------------------------------------------
 * Executing Game Code
 * ---------------------------------------------------------------------------
 */

gameInitialize();
snakeInitialize();
snakeInitialize2();
foodInitialize();
setInterval(gameLoop, 1500 / 30);

/*----------------------------------------------------------------------------
 * Game Functions
 * ---------------------------------------------------------------------------
 */

function gameInitialize() {
    var canvas = document.getElementById("game-screen");
    context = canvas.getContext("2d");

    screenWidth = window.innerWidth;
    screenHeight = window.innerHeight;

    canvas.width = screenWidth;
    canvas.height = screenHeight;

    document.addEventListener("keydown", keyboardHandler);
    document.addEventListener("keydown", keyboardHandler2);

    gameOverMenu = document.getElementById("gameOver");

    restartButton = document.getElementById("restartButton");
    restartButton.addEventListener("click", gameRestart);

    playHUD = document.getElementById("playHUD");

    scoreboard = document.getElementById("scoreboard");
    scoreboard2 = document.getElementById("scoreboard2");


    setState("PLAY");
}

function gameLoop() {
    gameDraw();
    drawScoreBoard();
    drawScoreBoard2();
    if (gameState == "PLAY") {
        snakeUpdate();
        snakeUpdate2();
        snakeDraw();
        snakeDraw2();
        foodDraw();

    }
}

function gameDraw() {
    context.fillStyle = "rgb(179, 0, 229)";
    context.fillRect(0, 0, screenWidth, screenHeight);
}

function gameRestart() {
    snakeInitialize();
    snakeInitialize2();
    foodInitialize();
    hideMenu(gameOverMenu);
    setState("PLAY");
}

/*----------------------------------------------------------------------------
 * Snake Functions
 * ---------------------------------------------------------------------------
 */

function snakeInitialize() {
    snake = [];
    snakeLength = 5;
    snakeSize = 25;
    snakeDirection = "down";

    for (var index = snakeLength + 1; index >= 0; index--) {
        snake.push({
            x: index,
            y: 0
        });
    }
}
function snakeInitialize2() {
    snake2 = [];
    snakeLength2 = 5;
    snakeSize2 = 25;
    snakeDirection2 = "down";

    for (var index = snakeLength2 + 1; index >= 0; index--) {
        snake2.push({
            x: 9,
            y: 7
        });
    }
}
function snakeDraw() {
    for (var index = 0; index < snake.length; index++) {

        context.strokeStyle = "#000000";
        context.strokeRect(snake[index].x * snakeSize, snake[index].y * snakeSize, snakeSize, snakeSize);
        context.linewidth = 10;

        context.fillStyle = "#FFFFFF";
        context.fillRect(snake[index].x * snakeSize, snake[index].y * snakeSize, snakeSize, snakeSize);
    }
}

function snakeDraw2() {
    for (var index = 0; index < snake2.length; index++) {

        context.strokeStyle = "#DE1600";
        context.strokeRect(snake2[index].x * snakeSize2, snake2[index].y * snakeSize2, snakeSize2, snakeSize2);
        context.linewidth = 10;

        context.fillStyle = "black";
        context.fillRect(snake2[index].x * snakeSize2, snake2[index].y * snakeSize2, snakeSize2, snakeSize2);
    }
}

function snakeUpdate() {
    var snakeHeadX = snake[0].x;
    var snakeHeadY = snake[0].y;

    if (snakeDirection == "down") {
        snakeHeadY++;
    }

    else if (snakeDirection == "right") {
        snakeHeadX++;
    }

    if (snakeDirection == "up") {
        snakeHeadY--;
    }

    else if (snakeDirection == "left") {
        snakeHeadX--;
    }
    checkFoodCollisions(snakeHeadX, snakeHeadY);
    /*
    checkWallCollisionsX(snakeHeadX, snakeHeadY);
    checkWallCollisionsY(snakeHeadX, snakeHeadY);
    */
    checkSnakeCollisions(snakeHeadX, snakeHeadY);
    var snakeTail = snake.pop();
    snakeTail.x = snakeHeadX;
    snakeTail.y = snakeHeadY;
    snake.unshift(snakeTail);
    testWallCollisions();
}
function snakeUpdate2() {
    var snakeHeadX2 = snake2[0].x;
    var snakeHeadY2 = snake2[0].y;

    if (snakeDirection2 == "down") {
        snakeHeadY2++;
    }

    else if (snakeDirection2 == "left") {
        snakeHeadX2++;
    }

    if (snakeDirection2 == "up") {
        snakeHeadY2--;
    }

    else if (snakeDirection2 == "right") {
        snakeHeadX2--;
    }


    checkFoodCollisions2(snakeHeadX2, snakeHeadY2);
    /*
    checkWallCollisionsX(snakeHeadX2, snakeHeadY2);
    checkWallCollisionsY(snakeHeadX2, snakeHeadY2);
    */
    checkSnakeCollisions(snakeHeadX2, snakeHeadY2);
    var snakeTail2 = snake2.pop();
    snakeTail2.x = snakeHeadX2;
    snakeTail2.y = snakeHeadY2;
    snake2.unshift(snakeTail2);
}
/*----------------------------------------------------------------------------
 * Food Functions
 * ---------------------------------------------------------------------------
 */

function foodInitialize() {
    food = {
        x: 0,
        y: 0
    };
    setFoodPosition();
}

function foodDraw() {
    context.fillstyle = "pink";
    context.fillRect(food.x * snakeSize, food.y * snakeSize, snakeSize, snakeSize);
}

function setFoodPosition() {
    var randomX = Math.floor(Math.random() * screenWidth);
    var randomY = Math.floor(Math.random() * screenHeight);

    food.x = Math.floor(randomX / snakeSize);
    food.y = Math.floor(randomY / snakeSize);
}

/*----------------------------------------------------------------------------
 * Input Functions
 * ---------------------------------------------------------------------------
 */
function keyboardHandler(event) {
    console.log(event);

    if (event.keyCode == "39" && snakeDirection != "left") {
        snakeDirection = "right";
    }

    if (event.keyCode == "82") {
        setState("PLAY");
        displayMenu(playHUD);
    }

    else if (event.keyCode == "40" && snakeDirection != "up") {
        snakeDirection = "down";
    }

    else if (event.keyCode == "38" && snakeDirection != "down") {
        snakeDirection = "up";
    }


    else if (event.keyCode == "37" && snakeDirection != "right") {
        snakeDirection = "left";
    }
}
function keyboardHandler2(event) {
    console.log(event);
    if (event.keyCode == "68" && snakeDirection2 != "right") {
        snakeDirection2 = "left";
    }

    else if (event.keyCode == "65" && snakeDirection2 != "left") {
        snakeDirection2 = "right";
    }

    else if (event.keyCode == "83" && snakeDirection2 != "up") {
        snakeDirection2 = "down";
    }

    else if (event.keyCode == "87" && snakeDirection2 != "down") {
        snakeDirection2 = "up";
    }
}

/*----------------------------------------------------------------------------
 * Collision Handling
 * ---------------------------------------------------------------------------
 */

function checkFoodCollisions(snakeHeadX, snakeHeadY) {
    if (snakeHeadX == food.x && snakeHeadY == food.y) {
        snake.push({
            x: 0,
            y: 0
        });
        snakeLength++;

        var audio = new Audio('Giggidi Giggidi Giggidi.mp3');
        audio.play();
        foodDraw();
        setFoodPosition();
    }
}

function checkFoodCollisions2(snakeHeadX2, snakeHeadY2) {
    if (snakeHeadX2 == food.x && snakeHeadY2 == food.y) {
        snake2.push({
            x: 0,
            y: 0
        });
        snakeLength2++;

        var audio = new Audio('Giggidi Giggidi Giggidi.mp3');
        audio.play();
        foodDraw();
        setFoodPosition();
    }
}

function testWallCollisions(snakeHeadX, snakeHeadY) {
    if (snakeHeadX * snakeSize >= screenWidth || snakeHeadX * snakeSize < 0) {
        snakeHeadX == screenWidth;
    }
}

/*

function checkWallCollisionsX(snakeHeadX, snakeHeadY) {
    if (snakeHeadX * snakeSize >= screenWidth || snakeHeadX * snakeSize < 0) {
        setState("GAMEOVER");
        var audio = new Audio('beep-01a.mp3');
        audio.play();
    }
}

function checkWallCollisionsY(snakeHeadX, snakeHeadY) {
    if (snakeHeadY * snakeSize >= screenHeight || snakeHeadY * snakeSize < 0) {
        setState("GAMEOVER");
        var audio = new Audio('beep-01a.mp3');
        audio.play();
    }
}
*/

function checkSnakeCollisions(snakeHeadX, snakeHeadY) {
    for (var index = 1; index < snake.length; index++) {
        if (snakeHeadX == snake[index].x && snakeHeadY == snake[index].y) {
            setState("GAMEOVER");
            var audio = new Audio('beep-01a.mp3');
            audio.play();
            return;

        }

    }
}
/*
function checkSnakeCollisions2(snakeHeadX2, snakeHeadY2) {
    for (var index = 1; index < snake2.length; index++) {
        if (snakeHeadX == snake2[index].x && snakeHeadY == snake2[index].y) {
            setState("GAMEOVER");
            var audio = new Audio('beep-01a.mp3');
            audio.play();
            return;

        }

    }
}
*/

/*----------------------------------------------------------------------------
 * Game State Handling
 * ---------------------------------------------------------------------------
 */

function setState(state) {
    gameState = state;
    showMenu(state);
}

/*----------------------------------------------------------------------------
 * Menu Functions
 * ---------------------------------------------------------------------------
 */

function displayMenu(menu) {
    menu.style.visibility = "visible";
}

function hideMenu(menu) {
    menu.style.visibility = "hidden";
}

function showMenu(state) {
    if (state == "GAMEOVER") {
        displayMenu(gameOverMenu);
    }
    else if (state == "PLAY") {
        displayMenu(playHUD);
    }
}

function drawScoreBoard() {
    scoreboard.innerHTML = "WhiteLength: " + snakeLength;

}

function drawScoreBoard2() {
    scoreboard.innerHTML = "BlackLength: " + snakeLength2;
}